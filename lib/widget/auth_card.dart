import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:shop_app/model/enum/AuthMode.dart';
import 'package:shop_app/models/http_exception.dart';
import 'package:shop_app/providers/auth.dart';

class AuthCard extends StatefulWidget {
  const AuthCard({Key key}) : super(key: key);

  @override
  _AuthCardState createState() => _AuthCardState();
}

class _AuthCardState extends State<AuthCard>
    with SingleTickerProviderStateMixin {
  // MIXIN THAT ADDS SOME METHODS TO THE CLASS THAT PROVIDES RESOURCES TO SET UP THE ANIMATION CONTROLLER
  final GlobalKey<FormState> _formKey = GlobalKey();
  AuthMode _authMode = AuthMode.LOGIN;
  Map<String, String> _authData = {"email": "", "password": ""};

  var _isLoading = false;
  final _passwordController = TextEditingController();

  AnimationController _controller;
  Animation<Offset> _slideAnimation;
  Animation<double> _opacityAnimation;

  @override
  void initState() {
    super.initState();
    _controller = AnimationController(
      vsync: this, // THIS "this" IS PROVIDED BY THE MIXIN.
      duration: Duration(
        milliseconds: 300,
      ),
    );

    // ANIMATION FOR THE SLIDING OF THE "Confirm Password" FIELD
    _slideAnimation = Tween<Offset>(
      // SETTING UP THE IN-BETWEEN ANIMATION
      begin: Offset(0.0, -1.0),
      end: Offset(0.0, 0.0),
    ).animate(
      CurvedAnimation(
        // EVERYTIME THE _controller ANIMATION TRIGGERS, THE _slideAnimation ANIMATES
        parent: _controller,
        curve: Curves.linear,
      ),
    );

    // ANIMATION FOR THE OPACITY OF THE "Confirm Password" FIELD
    _opacityAnimation = Tween(begin: 0.0, end: 1.0).animate(
      CurvedAnimation(
        parent: _controller,
        curve: Curves.easeIn,
      ),
    );
  }

  @override
  void dispose() {
    super.dispose();
    _controller.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final deviceSize = MediaQuery.of(context).size;
    final theme = Theme.of(context);
    return Card(
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.circular(10.0),
      ),
      elevation: 8.0,
      child: AnimatedContainer( // ANIMATES JUST THE CONTAINER
        // AUTOMATICALLY DETECTS THE SCREEN VALUE CHANGE AND ADDS THE ANIMATION
        duration: Duration(milliseconds: 300),
        curve: Curves.easeIn,
        height: _authMode == AuthMode.SIGN_UP ? 320 : 260,
        constraints: BoxConstraints(
          minHeight: _authMode == AuthMode.SIGN_UP ? 320 : 260,
        ),
        width: deviceSize.width * 0.75,
        padding: const EdgeInsets.all(16.0),
        child: Form(
          key: _formKey,
          child: SingleChildScrollView(
            child: Column(
              children: [
                TextFormField(
                  decoration: const InputDecoration(labelText: "E-mail"),
                  keyboardType: TextInputType.emailAddress,
                  validator: (value) {
                    if (value.isEmpty || !value.contains('@')) {
                      return "Invalid email";
                    }
                    return null;
                  },
                  onSaved: (value) {
                    _authData['email'] = value;
                  },
                ),
                TextFormField(
                  decoration: const InputDecoration(labelText: "Password"),
                  obscureText: true,
                  // STARS HIDING THE PASSWORD
                  controller: _passwordController,
                  validator: (value) {
                    if (value.isEmpty || value.length < 5) {
                      return "Password is too short";
                    }
                    return null;
                  },
                  onSaved: (value) {
                    _authData['password'] = value;
                  },
                ),
                AnimatedContainer(
                  constraints: BoxConstraints( // THIS CONTAINER WILL BE BETWEEN 60 AND 120 PIXELS HIGH IN SIGN UP MODE
                    minHeight: _authMode == AuthMode.SIGN_UP ? 60 : 0,
                    maxHeight: _authMode == AuthMode.SIGN_UP ? 120 : 0,
                  ),
                  duration: Duration(milliseconds: 300),
                  curve: Curves.easeIn,
                  child: FadeTransition( // THIS TRANSITION SERVES FOR THE Confirm Password FIELD DISPLAY ANIMATION
                    opacity: _opacityAnimation,
                    child: SlideTransition( // THE Confirm Password FIELD WILL FADE AND SLIDE AT THE SAME TIME
                      position: _slideAnimation,
                      child: TextFormField(
                        enabled: _authMode == AuthMode.SIGN_UP,
                        decoration:
                            const InputDecoration(labelText: "Confirm Password"),
                        obscureText: true, // STARS HIDING THE PASSWORD
                        validator: _authMode == AuthMode.SIGN_UP
                            ? (value) {
                                if (value != _passwordController.text) {
                                  return "Passwords do not match";
                                }
                                return null;
                              }
                            : null,
                      ),
                    ),
                  ),
                ),
                SizedBox(
                  height: 15,
                ),
                if (_isLoading)
                  CircularProgressIndicator()
                else
                  ElevatedButton(
                    child: Padding(
                      padding: const EdgeInsets.symmetric(
                        horizontal: 30.0,
                        vertical: 8.0,
                      ),
                      child: Text(
                        _authMode == AuthMode.LOGIN ? "LOGIN" : "SIGN UP",
                      ),
                    ),
                    onPressed: _submit,
                    style: ButtonStyle(
                      shape: MaterialStateProperty.all(
                        RoundedRectangleBorder(
                          borderRadius: BorderRadius.circular(30),
                        ),
                      ),
                      backgroundColor: MaterialStateProperty.all(
                        theme.colorScheme.primary,
                      ),
                      textStyle: MaterialStateProperty.all(
                        TextStyle(
                          color: theme.primaryTextTheme.button.color,
                        ),
                      ),
                    ),
                  ),
                TextButton(
                  child: Padding(
                    padding: const EdgeInsets.symmetric(
                      horizontal: 30.0,
                      vertical: 4,
                    ),
                    child: Text(
                      "${_authMode == AuthMode.LOGIN ? 'SIGNUP' : 'LOGIN'} INSTEAD",
                    ),
                  ),
                  onPressed: _switchAuthMode,
                  style: ButtonStyle(
                    textStyle: MaterialStateProperty.all(
                      TextStyle(color: theme.primaryColor),
                    ),
                    tapTargetSize: MaterialTapTargetSize.shrinkWrap,
                  ),
                ),
              ],
            ),
          ),
        ),
      ),
    );
  }

  Future<void> _submit() async {
    if (!_formKey.currentState.validate()) {
      return;
    }

    _formKey.currentState.save();

    _setStateDynamically(true);
    await handleLoginAndSignUp();
    _setStateDynamically(false);
  }

  Future<void> handleLoginAndSignUp() async {
    try {
      if (_authMode == AuthMode.LOGIN) {
        await Provider.of<Auth>(context, listen: false).logIn(
          _authData["email"],
          _authData["password"],
        );
      } else {
        await Provider.of<Auth>(context, listen: false).signUp(
          _authData["email"],
          _authData["password"],
        );
      }
    } on HttpException catch (error) {
      //CATCHING ERROR ON A SPECIFIC EXCEPTION CLASS

      switch (error.message) {
        case "EMAIL_EXISTS":
          _showErrorDialog("This email address is already in use!");
          break;
        case "INVALID_EMAIL":
          _showErrorDialog("This is not a valid email address!");
          break;
        case "WEAK_PASSWORD":
          _showErrorDialog("This password is too weak!");
          break;
        case "INVALID_PASSWORD":
          _showErrorDialog("This password is not valid!");
          break;
        case "EMAIL_NOT_FOUND":
          _showErrorDialog("Could not find a user with that email!");
          break;
        default:
          _showErrorDialog("Authentication failed");
      }
    } catch (error) {
      _showErrorDialog("Could not authenticate. Please try again later.");
    }
  }

  void _showErrorDialog(String errorMessage) {
    showDialog(
      context: context,
      builder: (ctx) => AlertDialog(
        title: Text("An Error Occurred!"),
        content: Text(errorMessage),
        actions: [
          TextButton(
            onPressed: () {
              Navigator.of(ctx).pop();
            },
            child: Text("Okay"),
          )
        ],
      ),
    );
  }

  void _setStateDynamically(bool stateBoolean) {
    setState(() {
      _isLoading = stateBoolean;
    });
  }

  void _switchAuthMode() {
    if (_authMode == AuthMode.LOGIN) {
      setState(() {
        _authMode = AuthMode.SIGN_UP;
      });

      _controller.forward();
    } else {
      setState(() {
        _authMode = AuthMode.LOGIN;
      });

      _controller.reverse();
    }
  }
}

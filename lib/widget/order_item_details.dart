import 'package:flutter/material.dart';
import 'package:shop_app/model/cart_item.dart';

class OrderItemDetails extends StatelessWidget {
  final CartItem prod;

  OrderItemDetails(this.prod);

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(horizontal: 15, vertical: 4),
      child: Row(
        mainAxisAlignment: MainAxisAlignment.spaceBetween,
        children: [
          Text(
            prod.title,
            style: TextStyle(
              fontSize: 18,
              fontWeight: FontWeight.bold,
            ),
          ),
          Text(
            "${prod.quantity}x \$${prod.price}",
            style: TextStyle(
              fontSize: 18,
              color: Colors.grey,
            ),
          ),
        ],
      ),
    );
  }
}

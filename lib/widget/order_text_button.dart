import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:shop_app/models/http_exception.dart';
import 'package:shop_app/providers/cart.dart';
import 'package:shop_app/providers/orders.dart';

class OrderTextButton extends StatefulWidget {
  const OrderTextButton({
    Key key,
    @required this.cart,
  }) : super(key: key);

  final Cart cart;

  @override
  State<OrderTextButton> createState() => _OrderTextButtonState();
}

class _OrderTextButtonState extends State<OrderTextButton> {
  var _isLoading = false;

  @override
  Widget build(BuildContext context) {
    return TextButton(
      child: _isLoading ? CircularProgressIndicator() : Text("ORDER NOW"),
      style: TextButton.styleFrom(primary: Theme.of(context).primaryColor),
      onPressed: (widget.cart.totalAmount <= 0 || _isLoading)
          ? null // IT DISABLES THE BUTTON
          : () async {
              _setStateDynamically(true);
              await _addOrderRequest(context);
              _setStateDynamically(false);
              widget.cart.clear();
            },
    );
  }

  Future<void> _addOrderRequest(BuildContext context) async {
    try {
      await Provider.of<Orders>(context, listen: false).addOrder(
        widget.cart.items.values.toList(),
        widget.cart.totalAmount,
      );
    } on HttpException catch (error) {
      showDialog(
        context: context,
        builder: (ctx) => AlertDialog(
          title: Text("An error occurred!"),
          content: Text(error.message),
          actions: [
            TextButton(
              onPressed: () => Navigator.of(context).pop(),
              child: Text("Okay"),
            )
          ],
        ),
      );
    }
  }

  void _setStateDynamically(bool value) {
    setState(() {
      _isLoading = value;
    });
  }
}

import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:shop_app/providers/auth.dart';
import 'package:shop_app/providers/cart.dart';
import 'package:shop_app/providers/product.dart';
import 'package:shop_app/screen/product_detail_screen.dart';

class ProductItem extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    final product = Provider.of<Product>(context, listen: false);
    final cart = Provider.of<Cart>(context, listen: false);
    final authData = Provider.of<Auth>(context, listen: false);

    return ClipRRect(// WRAPS THE CHILD WIDGET INTO A CERTAIN SHAPE
      borderRadius: BorderRadius.circular(10),
      child: GridTile(
        child: GestureDetector(
          child: Hero( //TRANSITION ANIMATION
            tag: product.id, // ANY OBJECT THAT WILL BE USED TO A CERTAIN HERO ANIMATION
            child: FadeInImage(// ANIMATION FOR DISPLAYING THE IMAGE
              placeholder: AssetImage(// PLACEHOLDER THAT WILL BE SHOWN WHEN THE IMAGE IS DOWNLOADING
                "lib/assets/images/product_placeholder.png",
              ),
              image: NetworkImage(// IMAGE PROVIDER THAT WILL DISPLAY THE URL IMAGE ONCE IT'S LOADED
                product.imageUrl,
              ),
              fit: BoxFit.cover,
            ),
          ),
          onTap: () {
            Navigator.of(context).pushNamed(
              ProductDetailScreen.routeName,
              arguments: product.id,
            );
          },
        ),
        footer: GridTileBar(
          // ADDS A BAR (IN THIS CASE AT THE BOTTOM OF THE WIDGET) WITH ANY COLOR
          backgroundColor: Colors.black87,
          title: Text(
            product.title,
            textAlign: TextAlign.center,
          ),
          leading: Consumer<Product>(
            builder: (ctx, product, _) => IconButton(
              //ADDS AN ICON TO THE LEFT OF THE TEXT
              icon: Icon(
                product.isFavorite ? Icons.favorite : Icons.favorite_border,
              ),
              onPressed: () =>
                  product.toggleFavoriteStatus(authData.token, authData.userId),
              color: Theme.of(context).accentColor,
            ),
          ),
          trailing: IconButton(
            // ADDS AN ICON TO THE RIGHT OF THE TEXT
            icon: Icon(Icons.shopping_cart),
            color: Theme.of(context).accentColor,
            onPressed: () {
              cart.addItem(product.id, product.price, product.title);
              Scaffold.of(context).hideCurrentSnackBar();
              Scaffold.of(context).showSnackBar(
                SnackBar(
                  content: Text(
                    "Item added to cart",
                  ),
                  duration: Duration(seconds: 2),
                  action: SnackBarAction(
                    label: "UNDO",
                    onPressed: () {
                      cart.removeSingleItem(product.id);
                    },
                  ),
                ),
              );
            },
          ),
        ),
      ),
    );
  }
}

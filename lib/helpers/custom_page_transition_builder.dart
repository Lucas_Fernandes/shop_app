import 'package:flutter/material.dart';

class CustomPageTransitionBuilder extends PageTransitionsBuilder {
  @override
  Widget buildTransitions<T>(
    PageRoute<T> route,
    BuildContext context,
    Animation<double> animation,
    Animation<double> secondaryAnimation,
    Widget child,
  ) {


    if (route.settings.name == "/") { // NO ANIMATION IN THE HOME PAGE
      return child;
    }

    return FadeTransition(
      opacity: animation,
      child: child,
    );
  }
}

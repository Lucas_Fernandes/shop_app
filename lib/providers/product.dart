import 'dart:convert';

import 'package:flutter/foundation.dart';
import 'package:http/http.dart' as http;

class Product with ChangeNotifier {
  final String id;
  final String title;
  final String description;
  final double price;
  final String imageUrl;
  bool isFavorite;

  Product({
    @required this.id,
    @required this.title,
    @required this.description,
    @required this.price,
    @required this.imageUrl,
    this.isFavorite = false,
  });

  Future<void> toggleFavoriteStatus(String authToken, String userId) async {
    final oldStatus = isFavorite;

    final updateUrl = Uri.https(
      "shop-app-flutter-e988f-default-rtdb.firebaseio.com",
      "/userFavorites/$userId/$id.json",
      {"auth": authToken},
    );

    isFavorite = !isFavorite;
    notifyListeners();

    try {
      final response = await http.put(
        updateUrl,
        body: json.encode(isFavorite),
      );

      if (response.statusCode >= 400) {
        _setFavValue(oldStatus);
      }
    } catch (error) {
      _setFavValue(oldStatus);
    }
  }

  void _setFavValue(bool oldStatus) {
    isFavorite = oldStatus;
    notifyListeners();
  }
}

import 'dart:convert';

import 'package:flutter/foundation.dart';
import 'package:http/http.dart' as http;
import 'package:shop_app/model/cart_item.dart';
import 'package:shop_app/model/order_item.dart';
import 'package:shop_app/models/http_exception.dart';

class Orders with ChangeNotifier {
  var _url;

  List<OrderItem> _orderItems = [];
  final String _userId;

  Orders(String _authToken, this._orderItems, this._userId) {
    _url = Uri.https(
      "shop-app-flutter-e988f-default-rtdb.firebaseio.com",
      "/orders/$_userId.json",
      {"auth": "$_authToken"}, // PASSING THE TOKEN AS A MAP IN QUERY PARAMETER
    );
  }

  List<OrderItem> get orders {
    return [..._orderItems];
  }

  Future<void> addOrder(List<CartItem> cartProducts, double total) async {
    final timestamp = DateTime.now();
    final response = await http.post(
      _url,
      body: json.encode(
        {
          'amount': total,
          'dateTime': timestamp.toIso8601String(),
          'products': cartProducts
              .map((cp) => {
                    'id': cp.id,
                    'title': cp.title,
                    'quantity': cp.quantity,
                    'price': cp.price
                  })
              .toList(),
        },
      ),
    );

    if (response.statusCode >= 400) {
      throw HttpException("Something went wrong! Please try again!");
    }

    _orderItems.insert(
      0,
      OrderItem(
        id: json.decode(response.body)['name'],
        amount: total,
        dateTime: timestamp,
        products: cartProducts,
      ),
    );

    notifyListeners();
  }

  Future<void> fetchAndSetOrders() async {
    final response = await http.get(_url);
    final List<OrderItem> loadedOrders = [];

    if (response.statusCode >= 400) {
      throw HttpException("Something went wrong! Please try again!");
    }

    final extractedData = json.decode(response.body) as Map<String, dynamic>;

    if (extractedData == null) {
      return;
    }

    extractedData.forEach(
      (orderId, orderData) {
        loadedOrders.add(
          OrderItem(
            id: orderId,
            amount: orderData['amount'],
            products: (orderData['products'] as List<dynamic>)
                .map(
                  (item) => CartItem(
                    id: item['id'],
                    title: item['title'],
                    quantity: item['quantity'],
                    price: item['price'],
                  ),
                )
                .toList(),
            dateTime: DateTime.parse(
              orderData['dateTime'],
            ),
          ),
        );
      },
    );

    _orderItems = loadedOrders.reversed.toList();
    notifyListeners();
  }
}

import 'dart:async';
import 'dart:convert';

import 'package:flutter/widgets.dart';
import 'package:http/http.dart' as http;
import 'package:shared_preferences/shared_preferences.dart';
import 'package:shop_app/models/http_exception.dart';

class Auth with ChangeNotifier {
  static const String _AUTO_LOGIN_KEY = "userData";
  static const _API_KEY = "AIzaSyBZWN3lZEEAwCTDohTKxV5XXz5oki5JY4s";
  static const _SIGN_UP_SEGMENT = "signUp";
  static const _SIGN_IN_SEGMENT = "signInWithPassword";

  String _token;
  DateTime _expiryDate;
  String _userId;
  Timer _authTimer;

  bool get isAuth {
    return token != null;
  }

  //VALIDATING IF THE TOKEN IS NOT EXPIRED (IF THE USER IS AUTHENTICATED).
  String get token {
    if (_expiryDate != null &&
        _expiryDate.isAfter(DateTime.now()) &&
        _token != null) {
      return _token;
    }
    return null;
  }

  String get userId {
    return _userId;
  }

  Future<void> signUp(String email, String password) async {
    // RETURNING THE FUTURE SO THE LOADING SPINNER WILL WORK PROPERLY
    return _authenticate(email, password, _SIGN_UP_SEGMENT, false);
  }

  Future<void> logIn(String email, String password) async {
    // RETURNING THE FUTURE SO THE LOADING SPINNER WILL WORK PROPERLY
    return _authenticate(email, password, _SIGN_IN_SEGMENT, true);
  }

  Future<void> _authenticate(
    String email,
    String password,
    String urlSegment,
    bool isLogin,
  ) async {
    final url = Uri.parse(
      "https://identitytoolkit.googleapis.com/v1/accounts:$urlSegment?key=$_API_KEY",
    );

    try {
      final response = await http.post(
        url,
        body: json.encode(
          {
            "email": email,
            "password": password,
            "returnSecureToken": true,
          },
        ),
      );

      final responseData = json.decode(response.body);

      if (responseData["error"] != null) {
        throw HttpException(responseData["error"]["message"]);
      }

      _token = responseData["idToken"];
      _userId = responseData["localId"];
      _expiryDate = DateTime.now().add(// CONVERTING THE RETURN STRING INTO SECONDS
        Duration(
          seconds: int.parse(
            responseData["expiresIn"],
          ),
        ),
      );

      if (isLogin) {
        _autoLogout();
      }

      notifyListeners(); //TRIGGERING THE CONSUMER IN THE main.dart.
      _storeInfoInSharedPreferences();
    } catch (error) {
      throw error;
    }
  }

  Future<bool> tryAutoLogin() async {
    final prefs = await _getSharedPrefsInstance();

    if (!prefs.containsKey(_AUTO_LOGIN_KEY)) {
      return false; // NO DATA STORED
    }

    final extractedUserData =
        json.decode(prefs.getString(_AUTO_LOGIN_KEY)) as Map<String, dynamic>;

    final expiryDateExtracted = DateTime.parse(extractedUserData["expiryDate"]);

    if (expiryDateExtracted.isBefore(DateTime.now())) {
      return false; // TOKEN EXPIRED
    }

    _token = extractedUserData["token"];
    _userId = extractedUserData["userId"];
    _expiryDate = expiryDateExtracted;

    notifyListeners();
    _autoLogout();

    return true; //TOKEN VALID
  }

  Future<void> logout() async {
    final prefs = await _getSharedPrefsInstance();

    _token = null;
    _userId = null;
    _expiryDate = null;

    _cancelTimer();
    notifyListeners();


    prefs.clear();
  }

  void _autoLogout() {
    final timeToExpiryInSecs = _expiryDate.difference(DateTime.now()).inSeconds;

    _authTimer = Timer(
      Duration(seconds: timeToExpiryInSecs),
      logout,
    );
  }

  void _cancelTimer() {
    if (_authTimer != null) {
      _authTimer.cancel();
      _authTimer = null;
    }
  }

  Future<SharedPreferences> _getSharedPrefsInstance() async {
    return await SharedPreferences.getInstance();
  }

  void _storeInfoInSharedPreferences() async {
    //GETTING SHARED PREFERENCES INSTANCE FOR AUTO LOGIN.
    final prefs = await _getSharedPrefsInstance();

    final userData = json.encode(
      {
        "token": _token,
        "userId": _userId,
        "expiryDate": _expiryDate.toIso8601String(),
      },
    );

    prefs.setString(_AUTO_LOGIN_KEY, userData);
  }
}

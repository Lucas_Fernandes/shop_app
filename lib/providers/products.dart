import 'dart:convert';

import 'package:flutter/material.dart';
import 'package:http/http.dart' as http;
import 'package:shop_app/models/http_exception.dart';
import 'package:shop_app/providers/product.dart';

class Products with ChangeNotifier {
  final String _authToken;
  final String _userId;
  var _url;

  List<Product> _items = [];

  Products(this._authToken, this._items, this._userId) {
    _url = Uri.https(
      "shop-app-flutter-e988f-default-rtdb.firebaseio.com",
      "/products.json",
      {
        "auth": _authToken,
        "orderBy": json.encode('creatorId'),
        "equalTo": json.encode(_userId),
      },
    );
  }

  List<Product> get items {
    return [..._items]; // RETURNING A COPY OF THE LIST.
  }

  List<Product> get favoriteItems {
    return _items.where((prodItem) => prodItem.isFavorite).toList();
  }

  Future<void> addProduct(Product product) async {
    try {
      final response = await http.post(
        _url,
        body: json.encode(
          {
            "title": product.title,
            "description": product.description,
            "imageUrl": product.imageUrl,
            "price": product.price,
            "creatorId": _userId
          },
        ),
      );

      final newProduct = Product(
        title: product.title,
        description: product.description,
        price: product.price,
        imageUrl: product.imageUrl,
        id: json.decode(response.body)["name"],
      );

      _items.add(newProduct);
      notifyListeners();
    } catch (error) {
      throw error;
    }
  }

  Future<void> updateProduct(Product newProduct) async {
    final prodIndex = _items.indexWhere((prod) => prod.id == newProduct.id);
    if (prodIndex >= 0) {
      //PRODUCT FOUND
      final updateUrl = Uri.https(
        "shop-app-flutter-e988f-default-rtdb.firebaseio.com",
        "/products/${newProduct.id}.json",
        {"auth": "$_authToken"},
      );

      final response = await http.patch(
        updateUrl,
        body: json.encode({
          "title": newProduct.title,
          "description": newProduct.description,
          "imageUrl": newProduct.imageUrl,
          "price": newProduct.price,
        }),
      );

      if (response.statusCode >= 400) {
        throw HttpException("Could not update the product. Please try again!");
      }

      _items[prodIndex] = newProduct;
      notifyListeners();
    } else {
      print("...");
    }
  }

  Future<void> deleteProduct(String productId) async {
    final deleteUrl = Uri.https(
        "shop-app-flutter-e988f-default-rtdb.firebaseio.com",
        "/products/$productId.json");

    final productIndex = _items.indexWhere((prod) => prod.id == productId);
    var product = _items[productIndex];

    _items.removeAt(productIndex);
    notifyListeners();

    final response = await http.delete(deleteUrl);
    if (response.statusCode >= 400) {
      _items.insert(productIndex, product);
      notifyListeners();
      throw HttpException("Could not delete product.");
    }

    product = null;
  }

  Product findById(String id) {
    return _items.firstWhere((prod) => prod.id == id);
  }

  //REQUEST METHOD WITH OPTIONAL PARAMETERS
  Future<void> fetchAndSetProducts([bool filterByUser = false]) async {
    final filterString = filterByUser
        ? _url
        : Uri.https("shop-app-flutter-e988f-default-rtdb.firebaseio.com",
            "/products.json", {"auth": _authToken});

    final favoriteUrl = Uri.https(
      "shop-app-flutter-e988f-default-rtdb.firebaseio.com",
      "/userFavorites/$_userId.json",
      {"auth": _authToken},
    );

    try {
      final response = await http.get(filterString);
      final extractedData = json.decode(response.body) as Map<String, dynamic>;
      final List<Product> loadedProducts = [];

      if (extractedData == null) {
        return;
      }

      //ANOTHER REQUEST THE RETRIEVES THE FAVORITE STATUS
      final favoriteData = json.decode((await http.get(favoriteUrl)).body);

      extractedData.forEach((prodId, prodData) {
        loadedProducts.add(Product(
          id: prodId,
          title: prodData["title"],
          description: prodData["description"],
          price: prodData["price"],
          isFavorite: //DOUBLE ?? CHECKS IF THE VALUE IS NULL OR NOT
              favoriteData == null ? false : favoriteData[prodId] ?? false,
          imageUrl: prodData["imageUrl"],
        ));
      });

      _items = loadedProducts;
      notifyListeners();
    } catch (error) {
      throw error;
    }
  }
}

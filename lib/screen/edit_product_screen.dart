import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:shop_app/models/http_exception.dart';
import 'package:shop_app/providers/product.dart';
import 'package:shop_app/providers/products.dart';

class EditProductScreen extends StatefulWidget {
  static const routeName = "/edit-product";

  @override
  _EditProductScreenState createState() => _EditProductScreenState();
}

class _EditProductScreenState extends State<EditProductScreen> {
  var _isInit = true;
  var _isLoading = false;
  final _imageUrlController = TextEditingController();
  final _imageUrlFocusNode = FocusNode();
  final _form = GlobalKey<FormState>();
  var _editedProduct = Product(
    id: null,
    title: "",
    price: 0,
    imageUrl: "",
    description: "",
  );
  var _initValues = {
    'title': '',
    'description': '',
    'price': '',
    'imageUrl': ''
  };

  @override
  void initState() {
    _imageUrlFocusNode.addListener(_updateImageUrl);
    super.initState();
  }

  @override
  void didChangeDependencies() {
    if (_isInit) {
      final productId = ModalRoute.of(context).settings.arguments as String;
      if (productId != null) {
        _editedProduct =
            Provider.of<Products>(context, listen: false).findById(productId);
        _initValues = {
          "title": _editedProduct.title,
          "description": _editedProduct.description,
          "price": _editedProduct.price.toString(),
          "imageUrl": "",
        };

        _imageUrlController.text = _editedProduct.imageUrl;
      }
    }
    _isInit = false;
    super.didChangeDependencies();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Edit Product"),
        actions: [IconButton(icon: Icon(Icons.save), onPressed: _saveForm)],
      ),
      body: _isLoading
          ? Center(
              child: CircularProgressIndicator(),
            )
          : Padding(
              padding: const EdgeInsets.all(16.0),
              child: Form(
                key: _form,
                child: SingleChildScrollView(
                  child: Column(
                    children: [
                      TextFormField(
                        initialValue: _initValues["title"],
                        decoration: InputDecoration(labelText: "Title"),
                        textInputAction: TextInputAction.next,
                        onSaved: (value) {
                          _editedProduct = _createProduct(
                            value,
                            _editedProduct.description,
                            _editedProduct.price,
                            _editedProduct.imageUrl,
                            _editedProduct.id,
                            _editedProduct.isFavorite,
                          );
                        },
                        validator: (value) {
                          if (value.isEmpty) {
                            return "Please provide a title!";
                          }
                          return null;
                        },
                      ),
                      TextFormField(
                        initialValue: _initValues["price"],
                        decoration: InputDecoration(labelText: "Price"),
                        textInputAction: TextInputAction.next,
                        keyboardType:
                            TextInputType.numberWithOptions(decimal: true),
                        onSaved: (value) {
                          _editedProduct = _createProduct(
                            _editedProduct.title,
                            _editedProduct.description,
                            double.parse(value),
                            _editedProduct.imageUrl,
                            _editedProduct.id,
                            _editedProduct.isFavorite,
                          );
                        },
                        validator: (value) {
                          if (value.isEmpty) {
                            return "Please provide a price!";
                          }

                          if (double.tryParse(value) == null) {
                            return "Please enter a valid number!";
                          }

                          if (double.parse(value) <= 0) {
                            return "Please enter a number greater than zero!";
                          }

                          return null;
                        },
                      ),
                      TextFormField(
                        initialValue: _initValues["description"],
                        decoration: InputDecoration(labelText: "Description"),
                        maxLines: 3,
                        keyboardType: TextInputType.multiline,
                        onSaved: (value) {
                          _editedProduct = _createProduct(
                            _editedProduct.title,
                            value,
                            _editedProduct.price,
                            _editedProduct.imageUrl,
                            _editedProduct.id,
                            _editedProduct.isFavorite,
                          );
                        },
                        validator: (value) {
                          if (value.isEmpty) {
                            return "Please provide a description!";
                          }

                          if (value.length < 10) {
                            return "Should be at least 10 characters long!";
                          }
                          return null;
                        },
                      ),
                      Row(
                        crossAxisAlignment: CrossAxisAlignment.end,
                        children: [
                          Container(
                            width: 100,
                            height: 100,
                            margin: EdgeInsets.only(top: 8, right: 10),
                            decoration: BoxDecoration(
                              border: Border.all(
                                width: 1,
                                color: Colors.grey,
                              ),
                            ),
                            child: _imageUrlController.text.isEmpty
                                ? Text("Enter a URL")
                                : FittedBox(
                                    child: Image.network(
                                      _imageUrlController.text,
                                    ),
                                    fit: BoxFit.fill,
                                  ),
                          ),
                          Expanded(
                            child: TextFormField(
                              decoration:
                                  InputDecoration(labelText: "Image URL"),
                              keyboardType: TextInputType.url,
                              textInputAction: TextInputAction.done,
                              controller: _imageUrlController,
                              focusNode: _imageUrlFocusNode,
                              onFieldSubmitted: (_) {
                                _saveForm();
                              },
                              onSaved: (value) {
                                _editedProduct = _createProduct(
                                  _editedProduct.title,
                                  _editedProduct.description,
                                  _editedProduct.price,
                                  value,
                                  _editedProduct.id,
                                  _editedProduct.isFavorite,
                                );
                              },
                              validator: (value) {
                                return _validateImageUrl(value);
                              },
                            ),
                          ),
                        ],
                      ),
                    ],
                  ),
                ),
              ),
            ),
    );
  }

  String _validateImageUrl(String url) {
    if (url.isEmpty) {
      return "Please provide an image url!";
    }
    if (!url.startsWith("https://") && !url.startsWith("http://")) {
      return "Invalid URL!. Please provide a valid one";
    }
    return null;
  }

  Product _createProduct(
    String title,
    String description,
    double price,
    String imageUrl,
    String id,
    bool isFavorite,
  ) {
    return Product(
      id: id,
      title: title,
      description: description,
      price: price,
      imageUrl: imageUrl,
      isFavorite: isFavorite,
    );
  }

  void _updateImageUrl() {
    if (!_imageUrlFocusNode.hasFocus) {
      setState(() {});
    }
  }

  Future<void> _saveForm() async {
    if (!_form.currentState.validate()) {
      //VALIDATES THE FORM
      return;
    }

    _form.currentState.save();
    setState(() {
      _isLoading = true;
    });

    if (_editedProduct.id != null) {
      await updateProductRequest();
    } else {
      await addProductRequest();
    }

    setState(() {
      _isLoading = false;
    });

    Navigator.of(context).pop();
  }

  Future<void> updateProductRequest() async {
    try {
      await Provider.of<Products>(context, listen: false)
          .updateProduct(_editedProduct);
    } on HttpException catch (error) {
      await showDialog(
        context: context,
        builder: (ctx) => AlertDialog(
          title: Text("An error occurred!"),
          content: Text(error.message),
          actions: [
            TextButton(
              onPressed: () {
                Navigator.of(context).pop();
              },
              child: Text("Okay"),
            ),
          ],
        ),
      );
    }
  }

  Future<void> addProductRequest() async {
    try {
      await Provider.of<Products>(context, listen: false)
          .addProduct(_editedProduct);
    } catch (error) {
      await showDialog(
        context: context,
        builder: (ctx) => AlertDialog(
          title: Text("An error occurred!"),
          content: Text("Something went wrong! Please try again!"),
          actions: [
            TextButton(
              child: Text("Okay"),
              onPressed: () {
                Navigator.of(ctx).pop(); //CLOSES THE DIALOG
              },
            ),
          ],
        ),
      );
    }
  }

  @override
  void dispose() {
    _imageUrlController.dispose();
    _imageUrlFocusNode.removeListener(_updateImageUrl);
    super.dispose();
  }
}

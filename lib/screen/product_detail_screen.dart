import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:shop_app/providers/products.dart';

class ProductDetailScreen extends StatelessWidget {
  static const String routeName = "/product-details";

  @override
  Widget build(BuildContext context) {
    final productId = ModalRoute.of(context).settings.arguments;
    final loadedProduct =
        Provider.of<Products>(context, listen: false).findById(productId);

    return Scaffold(
      body: CustomScrollView(// ALLOWS CUSTOM SCROLL BEHAVIOUR
        slivers: [ // SLIVERS ARE SCROLLABLE AREAS ON THE SCREEN
          SliverAppBar(// ALLOWS THE COLLAPSING TOOLBAR EFFECT
            expandedHeight: 300, // HEIGHT AT FULL SIZE.
            pinned: true,
              flexibleSpace: FlexibleSpaceBar( // CONTENT INSIDE THE APP BAR THAT WILL BE SHOWN ONCE IT'S EXPANDED
                title: Text(loadedProduct.title),
                background: Hero(
                  tag: loadedProduct.id,
                  child: Image.network(
                    loadedProduct.imageUrl,
                    fit: BoxFit.cover,
                  ),
                ),
            ),
          ),
          SliverList(// ALLOWS PART OF THE SCREEN CONTENT TO SCROLL
            delegate: SliverChildListDelegate(
              [
                SizedBox(
                  height: 10,
                ),
                Text(
                  "\$${loadedProduct.price}",
                  style: TextStyle(
                    color: Colors.grey,
                    fontSize: 20,
                  ),
                  textAlign: TextAlign.center,
                ),
                SizedBox(
                  height: 10,
                ),
                Container(
                  padding: EdgeInsets.symmetric(horizontal: 10),
                  child: Text(
                    loadedProduct.description,
                    textAlign: TextAlign.center,
                    softWrap: true,
                  ),
                ),
                SizedBox(
                  height: 800,
                )
              ],
            ),
          ),
        ],
      ),
    );
  }
}

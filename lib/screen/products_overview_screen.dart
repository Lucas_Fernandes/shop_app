import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:shop_app/enums/FilterOptions.dart';
import 'package:shop_app/providers/cart.dart';
import 'package:shop_app/providers/products.dart';
import 'package:shop_app/screen/cart_screen.dart';
import 'package:shop_app/widget/app_drawer.dart';
import 'package:shop_app/widget/badge.dart';
import 'package:shop_app/widget/products_grid.dart';

class ProductsOverviewScreen extends StatefulWidget {
  @override
  _ProductsOverviewScreenState createState() => _ProductsOverviewScreenState();
}

class _ProductsOverviewScreenState extends State<ProductsOverviewScreen> {
  var _showOnlyFavorites = false;
  var _isInit = true;
  var _isLoading = false;

  @override
  void didChangeDependencies() {
    // THIS METHOD RUNS MULTIPLE TIMES, SO IT NEEDS A VERIFICATION.
    if (_isInit) {
      _fetchProductsRequest();
    }

    _isInit = false;
    super.didChangeDependencies();
  }

  void _setStateDynamically(bool state) {
    setState(() {
      // STATE CONTROL CHANGE TO SHOW A LOADING SPINNER
      _isLoading = state;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text("MyShop"),
        actions: [
          PopupMenuButton(
            onSelected: (FilterOptions selectedValue) {
              changeFilter(selectedValue);
            },
            icon: Icon(Icons.more_vert),
            itemBuilder: (_) => [
              PopupMenuItem(
                child: Text("Only favorites"),
                value: FilterOptions.FAVORITES,
              ),
              PopupMenuItem(
                child: Text("Show All"),
                value: FilterOptions.ALL,
              ),
            ],
          ),
          Consumer<Cart>(
            builder: (_, cartData, ch) => Badge(
              color: null,
              child: ch,
              value: cartData.itemCount.toString(),
            ),
            child: IconButton(
              icon: Icon(Icons.shopping_cart),
              onPressed: () {
                Navigator.of(context).pushNamed(CartScreen.routeName);
              },
            ),
          ),
        ],
      ),
      drawer: AppDrawer(),
      body: _isLoading
          ? Center(child: CircularProgressIndicator())
          : RefreshIndicator(
              onRefresh: () => _fetchProductsRequest(),
              child: ProductsGrid(_showOnlyFavorites),
            ),
    );
  }

  void changeFilter(FilterOptions selectedValue) {
    setState(() {
      if (selectedValue == FilterOptions.FAVORITES) {
        _showOnlyFavorites = true;
      } else {
        _showOnlyFavorites = false;
      }
    });
  }

  Future<void> _fetchProductsRequest() async {
    try {
      _setStateDynamically(true);
      await Provider.of<Products>(context, listen: false)
          .fetchAndSetProducts()
          .then(
            (value) => _setStateDynamically(false),
          );
    } catch (error) {
      await showDialog(
        context: context,
        builder: (ctx) => AlertDialog(
          title: Text("An error occurred!"),
          content: Text("Something went wrong! Please try again!"),
          actions: [
            TextButton(
              onPressed: () => Navigator.of(context).pop(),
              child: Text("Okay"),
            )
          ],
        ),
      );
      _setStateDynamically(false);
    }
  }
}
